# Vivado-docker

This repo contains stuff to build an Docker image with Vivado.

# building

You first need to download the full Vivado installer (Xilinx_Vivado_SDK_2018.3_1207_2324.tar.gz, ~19GB) in this folder. Then run `./build.sh`, and wait.

You should now have an docker image named vivado.

# running

See the provided `run.sh` and `run_with_pwd.sh` script to some examples of how to run an ephemeral and interactive container. The first argument is the command to run in the container. By default, this is a shell, but you can also run `vivado`.
