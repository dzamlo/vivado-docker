FROM ubuntu:16.04

RUN apt-get update && \
    apt-get install -y --no-install-recommends \
        curl \
        libfontconfig1 \
        libfreetype6 \
        libglib2.0-0 \
        libsm6 \
        libx11-6 \
        libxrender1 \
        libxext6 \
        libxi6 \
        libxtst6 \
        && \
    rm -rf /var/lib/apt

RUN curl -v http://localhost:8000/Xilinx_Vivado_SDK_2018.3_1207_2324.tar.gz | tar xzf - && \
    cd Xilinx_Vivado_SDK_2018.3_1207_2324 && \
    ./xsetup -optimize_diskspace --xdebug --agree 3rdPartyEULA,WebTalkTerms,XilinxEULA --batch Install -e 'Vivado HL WebPACK' -l /opt/Xilinx && \
    cd .. && \
    rm -r Xilinx_Vivado_SDK_2018.3_1207_2324
    
ENV PATH $PATH:/opt/Xilinx/Vivado/2018.3/bin/

