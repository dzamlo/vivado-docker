#! /bin/sh

set -o errexit

trap 'kill $(jobs -p)' EXIT
python3 -m http.server &
sleep 1
docker build . --network=host -t vivado
