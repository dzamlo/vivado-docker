#! /bin/sh
docker run -it --rm --net=host --env="DISPLAY" --volume="$HOME/.Xauthority:/root/.Xauthority:ro" -u `id -u $USER`:`id -g $USER` --volume="$PWD:/volume" vivado "$@"
